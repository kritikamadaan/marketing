export const  backStyle = 
{
cursor: "pointer", 
position: "absolute", 
right: "10px", 
top: "10px", 
fontSize: "20px"
}

export const inputControl=
{
margin:"10px 40px",
alignContent:"center",
width:"100%"
}
export const selectControl={
    margin:"10px  50px",
    alignContent:"center",
    width:"80%"
}
export const textControl={
    width:"80%"
}
export const buttonStyle={
    width:"240%",
    margin:"30px"
}
export const fontStyle={
    fontWeight:"bold"
}
