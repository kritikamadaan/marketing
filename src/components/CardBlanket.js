import React, { Component } from 'react'
import "./CardBlanket.css";
import Quality from '../Cards/Quality/Quality';
import Market from '../Cards/Market/Market';
import Unrealized from '../Cards/Unrealised/Unrealized';
import Button from '../Cards/Button/Button';

class CardBlanket extends Component {
    render() {
        return (
            <div>
              <div className="card-column">
              <div className="col-lg-2">
              <div className="MuiPaper-root MuiPaper-elevation1 MuiPaper-rounded" elevate="4">
              <div className="Column1">
              <i className="fa fa-bars"></i>
              <div class="Column1_2">
              <span class="Column1_2_1">AADR</span>
              <span class="Column1_2_2">$50.3</span></div>
              <div class="Column1_3">
              <span class="Column1_3_1">iShares</span>
              <span class="Column1_3_2">by BlackDoc</span>
              <span class="Column1_3_3">S& P 500 Index</span>
              <span class="Column1_3_4">US Equity</span></div></div></div>
              </div>
              <div className="col-md-3">
              <Quality/>
              </div>
              <div className="col-md-3">
              <Market/>
              </div>
              <div className="col-lg-3">
              <Unrealized/>
              </div>
              <div className="col-lg-2">
              <Button/>
              </div>
              </div>  
            </div>
        )
    }
}

export default CardBlanket
