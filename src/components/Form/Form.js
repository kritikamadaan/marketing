import React, { Component } from 'react'
import TextField from '@material-ui/core/TextField';
import "./Form.css";
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import {inputControl,selectControl,textControl,buttonStyle} from "../../utils/Style";
import Button from '@material-ui/core/Button';

class Form extends Component {
    render() {
        return (
            <div>
            <form>
            <FormControl 
             style={selectControl}
             variant="filled"
             className="form-group">
            <InputLabel id="select">Select...</InputLabel>
            <Select
              className="form-control"
              labelId="Select..."
              id="demo"
              value=""
              onChange=""
            >
              <MenuItem value="">
                <em>Select</em>
              </MenuItem>
              <MenuItem value={10}>AADR</MenuItem>
              <MenuItem value={20}>MFEM</MenuItem>
              <MenuItem value={30}>JPEM</MenuItem>
              <MenuItem value={10}>KEMQ</MenuItem>
              <MenuItem value={20}>KLDW</MenuItem>
              <MenuItem value={30}>KOIN</MenuItem>
            </Select>
          </FormControl>
            <div 
            className="form-group" 
            style={inputControl}>
              <TextField 
              style={textControl}
              className="form-control"
               id="outlined-search"
               label="Price" 
               type="search" 
               variant="outlined" />
            </div>
             <div
             style={inputControl}
              className="form-group">
              <TextField 
              style={textControl}
              className="form-control"
              id="outlined-search"
               label="Quantity" 
               type="search" 
               variant="outlined" />
            </div>
            <div className="form-group"
            style={inputControl}>
            <TextField 
            style={textControl}
            className="form-control"
            id="outlined-search" 
            label="Invested Amount"
             type="search"
              variant="outlined" />
          </div>
          <FormControl>
          <Button  
          style={buttonStyle}
          className="button"
          type="submit" 
          variant="contained" 
          color="primary">
            Submit
          </Button>
          </FormControl>
          </form>  
            </div>
        )
    }
}

export default Form
