import React from 'react'
import Header from '../Layout/Header';

function TopNav() {
    return (
        <div>
        <section class="top-nav">
            <nav class="navbar navbar-expand-lg py-0">
                <div class="container">
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="exCollapsingNavbar2">
                      <Header/>
                    </div>
                </div>
            </nav>
        </section>
    
        </div>
    )
}

export default TopNav
