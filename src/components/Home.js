import React, { Component } from 'react'
import CardBlanket from './CardBlanket'
import DonutCharts from './DonutChart/DonutCharts'



class Home extends Component {
    render() {
        return (
            <div className="containers">
              <div className="row">
              <div className="col-lg-9">
             <CardBlanket/>
             <CardBlanket/>
             <CardBlanket/>
             <CardBlanket/>
             <CardBlanket/>
             <CardBlanket/>
              </div>
              <div className="col-lg-3">
                <DonutCharts/>
              </div>
              </div>
            </div>
        )
    }
}

export default Home
