import React, { Component } from 'react'
import DonutChart from 'react-d3-donut';
import Card from '@material-ui/core/Card';



class DonutCharts   extends Component {

        data = [{
            count:  '20',     
            color: 'rgb(3, 169, 244) ', 
            name: 'Mutual funds'
            },
            {
                count:  '20',     
                color: 'rgb(174, 156, 70)', 
                name: 'ETFs'
            },
        ]
    
    render() {
        return (
            <div>
            <Card>
            <h5>Portfolio</h5>
            <p>Assetwise</p>
            <DonutChart
            innerRadius={80}
            outerRadius={100}
            transition={true}
            svgClass="example6"
            pieClass="pie6"
            displayTooltip={true}
            strokeWidth={3}
            data={this.data} />
            <p>Mutual Funds:</p>
            <p>ETFs:</p>  
            </Card>
            </div>
        )
    }
}

export default DonutCharts
