import React from 'react';
import Home from './components/Home';
import TopNav from './components/TopNav';


function App() {
  return (
    <div>
        <TopNav/>
         <Home/>
    </div>
  );
}

export default App;
