import React, { Component } from 'react'
import Sidebar from './Sidebar/Sidebar'
import {fontStyle} from "../utils/Style";

class Header extends Component {
    
     state={
         sidebar:false
     }
     sidebarHandler = () => {
         console.log('sidebar handler clicked')
         this.setState({
             sidebar:true
         })
     }
    render() {
        return (
            <div className="header-container">
                <img  className="darts" src="/image1.jfif" height="24px" alt="header"/>
                <span className="bars" style={{float:"right"}}>
                 <Sidebar/> 
                 </span>
            </div>
        )
    }
}

export default Header
