import React, { Component } from 'react'

class Button extends Component {
    render() {
        return (
            <div >
            <div className="Button5">
            <button className="Button-outlined" style={{width:"60%"}}  type="button">
            <span className="Button-label">BUY</span>
            <span className="Button-root"></span>
            </button>
            <button className="Button-outlined"  style={{width:"60%"}}  type="button">
            <span className="Button-label">SELL</span>
            <span className="Button-root"></span></button>
            </div>  
            </div>
        )
    }
}

export default Button
