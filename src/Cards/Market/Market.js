import React, { Component } from 'react'
import "./Market.css";
class Market extends Component {
    render() {
        return (
            <div>
            <div className="MuiPaper-root MuiPaper-elevation1 MuiPaper-rounded">
            <div className="Column3_column3__3Gwhf">
            <div className="Column3_marketValue__C9vyQ">
            <span>Market Value</span>
            <span>$21629</span>
            </div>
            <div className="Column3_portfolioValue__DgndM">
            <span>% of portfolio value</span>
            <span><b>22.06%</b></span>
            </div><div>
            <div className="MuiLinearProgress-root jss133 jss132 MuiLinearProgress-colorSecondary MuiLinearProgress-determinate" 
            role="progressbar" aria-valuenow="22">
            <progress id="file" value="32" max="100"> 32% </progress>
            <div className="MuiLinearProgress-bar jss134 MuiLinearProgress-barColorSecondary MuiLinearProgress-bar1Determinate"
             style={{transform: "translateX(-77.94%)"}}>
             
            </div></div></div></div></div>  
            </div>
        )
    }
}

export default Market
