import React, { Component } from 'react'
import "./Unrealized.css";

class Unrealized extends Component {
    render() {
        return (
            <div>
            <div className="MuiPaper-root MuiPaper-elevation1 MuiPaper-rounded">
            <div class="Column4_column4__1QQvb">
            <div class="Column4_unrealizedPL__2O56_">
            <span>Unrealized P/L</span>
            <span className="fr">3676.93</span></div>
            <div className="Column4_return__1IWhX">
            <span>% Return</span><span><i className="fa fa-caret-up" style={{color: "rgb(7, 196, 7)"}}></i>&nbsp;20.48%</span></div>
            <div className="Column4_progress___6IXQ">
            <progress
            
             id="file" value="32" max="100"> 32% </progress>
           
            </div></div></div> 
            </div>
        )
    }
}

export default Unrealized
