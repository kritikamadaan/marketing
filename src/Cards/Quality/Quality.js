import React, { Component } from 'react'
import {fontStyle} from "../../utils/Style";
import "./Quality.css";


class Quality extends Component {
    render() {
        return (
            <div>
            <div className="MuiPaper-root MuiPaper-elevation1 MuiPaper-rounded" elevate="4">
            <div className="Column2_column2">
            <div className="Column2_quantity">
            <div className="Column2_left">
            <span><i class="fa fa-database"></i></span>
            <span>Quantity</span>
            </div><span style={fontStyle}>430</span>
            </div><div className="Column2_averageCost">
            <div className="Column2_left__y5HwO">
            <i class="fa fa-at" aria-hidden="true"></i><span>Avg. Cost</span></div>
            <span style={fontStyle}>41.75</span></div>
            <div className
            ="Column2_investedAmount">
            <div className="Column2_left__y5HwO">
            <i className='fas fa-money-bill'></i><span>Invested Amt</span></div>
            <span style={fontStyle}>$17952.07</span></div>
            </div></div>  
            </div>
        )
    }
}

export default Quality
